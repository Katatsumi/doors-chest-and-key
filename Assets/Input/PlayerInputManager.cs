//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.4.4
//     from Assets/Input/Player.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @PlayerInputManager : IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputManager()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Player"",
    ""maps"": [
        {
            ""name"": ""PlayerOnFoot"",
            ""id"": ""2a7e31f1-22c1-4270-b781-bc31c54a5782"",
            ""actions"": [
                {
                    ""name"": ""Crouch"",
                    ""type"": ""Button"",
                    ""id"": ""e0b9287b-81e6-4102-a28e-c5e73a4f146d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Walking"",
                    ""type"": ""Value"",
                    ""id"": ""415401f6-e6b6-4a9d-beee-d0faf3f01e4b"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""f98bf8ab-7d16-435f-b9e5-a47c62104080"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""b2254f7c-ed57-42fc-87df-da84ed186eaf"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""Rotate"",
                    ""type"": ""Value"",
                    ""id"": ""b2aa89ee-692c-409f-9807-a8039a8a8cb9"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""139714ab-df50-44d2-a8fe-d7dd9b70b146"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""f2b3463a-c40d-4706-a6aa-a68ae21eff71"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walking"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""dcf4bded-e9f3-4cd4-8af2-cd866ccea83c"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walking"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""31277f3c-f1f9-47a1-8d2d-1e99860d1a98"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walking"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""a7300e7c-ea37-4fe0-bdc7-488dc0cbe2bc"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walking"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""1917c528-6c53-4dee-a94c-d22cc12f1cd1"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walking"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""8b1f8f8e-c162-4ecd-8586-dc8020b309e4"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""393fe7de-8293-4e3f-a985-939d6c8c87e9"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""7cfc3b7e-6195-40ca-9842-1fad39b94b50"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""8be069d4-5c27-418f-bbd7-68754b931d45"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""4b59c2b0-dd0d-4a84-8491-d30bf66016e7"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // PlayerOnFoot
        m_PlayerOnFoot = asset.FindActionMap("PlayerOnFoot", throwIfNotFound: true);
        m_PlayerOnFoot_Crouch = m_PlayerOnFoot.FindAction("Crouch", throwIfNotFound: true);
        m_PlayerOnFoot_Walking = m_PlayerOnFoot.FindAction("Walking", throwIfNotFound: true);
        m_PlayerOnFoot_Interact = m_PlayerOnFoot.FindAction("Interact", throwIfNotFound: true);
        m_PlayerOnFoot_Look = m_PlayerOnFoot.FindAction("Look", throwIfNotFound: true);
        m_PlayerOnFoot_Rotate = m_PlayerOnFoot.FindAction("Rotate", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }
    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }
    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // PlayerOnFoot
    private readonly InputActionMap m_PlayerOnFoot;
    private IPlayerOnFootActions m_PlayerOnFootActionsCallbackInterface;
    private readonly InputAction m_PlayerOnFoot_Crouch;
    private readonly InputAction m_PlayerOnFoot_Walking;
    private readonly InputAction m_PlayerOnFoot_Interact;
    private readonly InputAction m_PlayerOnFoot_Look;
    private readonly InputAction m_PlayerOnFoot_Rotate;
    public struct PlayerOnFootActions
    {
        private @PlayerInputManager m_Wrapper;
        public PlayerOnFootActions(@PlayerInputManager wrapper) { m_Wrapper = wrapper; }
        public InputAction @Crouch => m_Wrapper.m_PlayerOnFoot_Crouch;
        public InputAction @Walking => m_Wrapper.m_PlayerOnFoot_Walking;
        public InputAction @Interact => m_Wrapper.m_PlayerOnFoot_Interact;
        public InputAction @Look => m_Wrapper.m_PlayerOnFoot_Look;
        public InputAction @Rotate => m_Wrapper.m_PlayerOnFoot_Rotate;
        public InputActionMap Get() { return m_Wrapper.m_PlayerOnFoot; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerOnFootActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerOnFootActions instance)
        {
            if (m_Wrapper.m_PlayerOnFootActionsCallbackInterface != null)
            {
                @Crouch.started -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnCrouch;
                @Crouch.performed -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnCrouch;
                @Crouch.canceled -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnCrouch;
                @Walking.started -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnWalking;
                @Walking.performed -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnWalking;
                @Walking.canceled -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnWalking;
                @Interact.started -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnInteract;
                @Look.started -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnLook;
                @Rotate.started -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnRotate;
                @Rotate.performed -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnRotate;
                @Rotate.canceled -= m_Wrapper.m_PlayerOnFootActionsCallbackInterface.OnRotate;
            }
            m_Wrapper.m_PlayerOnFootActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Crouch.started += instance.OnCrouch;
                @Crouch.performed += instance.OnCrouch;
                @Crouch.canceled += instance.OnCrouch;
                @Walking.started += instance.OnWalking;
                @Walking.performed += instance.OnWalking;
                @Walking.canceled += instance.OnWalking;
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @Rotate.started += instance.OnRotate;
                @Rotate.performed += instance.OnRotate;
                @Rotate.canceled += instance.OnRotate;
            }
        }
    }
    public PlayerOnFootActions @PlayerOnFoot => new PlayerOnFootActions(this);
    public interface IPlayerOnFootActions
    {
        void OnCrouch(InputAction.CallbackContext context);
        void OnWalking(InputAction.CallbackContext context);
        void OnInteract(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnRotate(InputAction.CallbackContext context);
    }
}
