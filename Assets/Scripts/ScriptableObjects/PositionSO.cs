using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PositionScriptableObject")]
public class PositionSO : ScriptableObject
{
    public float xMin, xMax;
    public float y;
    public float zMin,zMax;
     
}
