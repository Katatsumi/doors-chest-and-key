using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/RotationScriptableObject")]
public class RotationSO : ScriptableObject
{
    public Vector3 leftRotation;
    public Vector3 rightRotation;
    public Vector3 topRotation;
    public Vector3 bottomRotation;
}
