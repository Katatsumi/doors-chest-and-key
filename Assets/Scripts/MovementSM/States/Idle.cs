using UnityEngine;
public class Idle : BaseState
{
    public Idle(StateMachine stateMachine) : base (stateMachine) { }

    private PlayerInput playerInput;

    public override void StateEnter()
    {
        playerInput = GameObject.Find("Player").GetComponent<PlayerInput>();
        playerInput.playerOnFootActions.Crouch.performed +=ctx => machine.ChangeState(((MovementSM)machine).crouching);


    }
    public override void StateUpdateLogic()
    {
        if(playerInput.playerOnFootActions.Walking.ReadValue<Vector2>().x>0 ||
           playerInput.playerOnFootActions.Walking.ReadValue<Vector2>().y>0 ||
           playerInput.playerOnFootActions.Walking.ReadValue<Vector2>().x<0 ||
           playerInput.playerOnFootActions.Walking.ReadValue<Vector2>().y<0)
        {
            machine.ChangeState(((MovementSM)machine).moving);
        }
    }
    private void OnCrouchPerformed()
    {
        machine.ChangeState(((MovementSM)machine).crouching);
    }
    
}
