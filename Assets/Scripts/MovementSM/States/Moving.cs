using System;
using UnityEngine;
public class Moving : BaseState
{
    public Moving(StateMachine stateMachine) : base(stateMachine) { }

    private PlayerInput playerInput;
    private AudioManagerScript audioManager;

    public override void StateEnter()
    {
        playerInput = GameObject.Find("Player").GetComponent<PlayerInput>();
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManagerScript>();
        audioManager.SoundPlay("FootStep");

    }
    public override void StateUpdateLogic()
    {
        if (playerInput.playerOnFootActions.Walking.ReadValue<Vector2>().x == 0 && playerInput.playerOnFootActions.Walking.ReadValue<Vector2>().y == 0)
        {
            machine.ChangeState(((MovementSM)machine).idle);
        }
    }
    public override void StateExit()
    {
        audioManager.SoundStopPlaying("FootStep");
    }
}
