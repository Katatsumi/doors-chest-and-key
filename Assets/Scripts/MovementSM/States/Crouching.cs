using UnityEngine;
public class Crouching : BaseState
{
    public Crouching(MovementSM stateMachine) : base(stateMachine) { }
    private PlayerInput playerInput;

    public override void StateEnter()
    {
        playerInput = GameObject.Find("Player").GetComponent<PlayerInput>();
        playerInput.playerOnFootActions.Crouch.canceled += ctx => machine.ChangeState(((MovementSM)machine).idle);

    }
}
