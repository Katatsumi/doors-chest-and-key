using UnityEngine;
public class MovementSM : StateMachine
{
    [HideInInspector] public Idle idle;
    [HideInInspector] public Moving moving;
    [HideInInspector] public Crouching crouching;
    private void Awake()
    {
        idle = new Idle(this);
        moving = new Moving(this);
        crouching = new Crouching(this);
    }
    protected override BaseState GetInitialState()
    {
        return idle;
    }

}
