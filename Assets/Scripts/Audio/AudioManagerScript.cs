using UnityEngine;
using UnityEngine.Audio;

public class AudioManagerScript : MonoBehaviour
{
    public static AudioManagerScript audioManagerInstance;
    public Sound[] sounds;

    private void Awake()
    {
        foreach(Sound sound in sounds)
        {
            sound.audioSource=gameObject.AddComponent<AudioSource>();
            sound.audioSource.clip = sound.clip;
            sound.audioSource.volume = sound.volume;
            sound.audioSource.pitch=sound.pitch;
            if(sound.name=="FootStep")
                sound.audioSource.loop=true;
        }
    }
    private void Start()
    {
        if (audioManagerInstance != null)
            Destroy(gameObject);
        else
        {
            audioManagerInstance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    public void SoundPlay(string soundname)
    {
        foreach (Sound sound in sounds)
        {
            if (sound.name == soundname)
                sound.audioSource.Play();

        }

    }
    public void SoundStopPlaying(string soundname)
    {
        foreach (Sound sound in sounds)
        {
            if (sound.name == soundname)
                sound.audioSource.Stop();

        }
    }
}
