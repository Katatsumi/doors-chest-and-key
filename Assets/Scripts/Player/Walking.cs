using UnityEngine;

public class Walking : MonoBehaviour
{
    [SerializeField]
    private CharacterController controller;
    private Vector3 moveDirection;

    public float speed = 3f;

    public void PlayerWalking(Vector2 input)
    {
        moveDirection = Vector3.zero;
        moveDirection.x = input.x;
        moveDirection.z = input.y;
        controller.Move(transform.TransformDirection(moveDirection) * speed * Time.deltaTime);

    }
}