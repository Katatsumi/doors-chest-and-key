using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private PlayerInputManager playerInputManager;
    public PlayerInputManager.PlayerOnFootActions playerOnFootActions;

    [SerializeField] private Crouch crouch;
    [SerializeField] private Walking walking;
    [SerializeField] private PlayerLook playerLook;

    private void Awake()
    {
        playerInputManager=new PlayerInputManager();
        playerOnFootActions = playerInputManager.PlayerOnFoot;
    }
    private void OnEnable()
    {
        playerOnFootActions.Enable();
    }
    private void Start()
    {
        playerOnFootActions.Crouch.performed += ctx => crouch.PlayerCrouch();
        playerOnFootActions.Crouch.canceled += ctx => crouch.PlayerGetUp();
    }
    private void Update()
    {
        playerLook.ProcessLook(playerOnFootActions.Look.ReadValue<Vector2>());
    }
    private void FixedUpdate()
    {
        walking.PlayerWalking(playerOnFootActions.Walking.ReadValue<Vector2>());
    }
    private void OnDisable()
    {
        playerOnFootActions.Disable();
    }

}
