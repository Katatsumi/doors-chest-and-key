using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TeleportationCheck : MonoBehaviour
{
    [SerializeField] PlayerInput playerInput;
    [SerializeField] Interaction interaction;
    [SerializeField] TextMeshProUGUI interactionText;
    [SerializeField] MouseCursor mouseCursor;
  
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            mouseCursor.crosshairCursor();
            playerInput.enabled = true;
            interaction.enabled = true;
            interactionText.gameObject.SetActive(true);
        }
    }
}
