using UnityEngine;

public class Crouch : MonoBehaviour
{
    [SerializeField] private GameObject playerCamera;
    [SerializeField] private Walking walking;
    private float cameraCrouchPositiony = 0.3f;
    private float cameraStandingPositiony = 0.7f;

    public void PlayerCrouch()
    {
        playerCamera.transform.localPosition = new Vector3(playerCamera.transform.localPosition.x, cameraCrouchPositiony, playerCamera.transform.localPosition.z);
        walking.speed = 1f;
    }
    public void PlayerGetUp()
    {
        playerCamera.transform.localPosition = new Vector3(playerCamera.transform.localPosition.x, cameraStandingPositiony, playerCamera.transform.localPosition.z);
        walking.speed = 3f;
    }   
}
