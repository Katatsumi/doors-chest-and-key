using System;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    [SerializeField] private Camera mainCam;
    [SerializeField] private float cameraRotation;
    [SerializeField] private float xSensivity;
    [SerializeField] private float ySensivity;
    
    public void ProcessLook(Vector2 input)
    {
        float mouseX = input.x;
        float mouseY = input.y;
        cameraRotation -=(mouseY*Time.deltaTime)*ySensivity;
        cameraRotation=Mathf.Clamp(cameraRotation, -80f, 80f);
        mainCam.transform.localRotation = Quaternion.Euler(cameraRotation, 0, 0);
        transform.Rotate(Vector3.up * (mouseX * Time.deltaTime * xSensivity));
    }
  
}
