using UnityEngine;

public class Interactable : MonoBehaviour
{
    public string prompt;

    public void BaseInteract()
    {
        Interact();
    }
    protected virtual void Interact()
    {

    }

}
