using UnityEngine;

public class Interaction : MonoBehaviour
{
    [SerializeField] HooverText hooverText;
    [SerializeField] PlayerInput input;
    [SerializeField] LayerMask interactableMask;
    private float rangeRaycast = 2f;
    private Transform selectedObject;

    void Update()
    {
        hooverText.UpdateHooverText(string.Empty);

        if (selectedObject != null)
        {
            var selectionRenderer=selectedObject.GetComponent<Renderer>();
            selectionRenderer.material.color = Color.white;
            selectedObject = null;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin,ray.direction*10,Color.yellow);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, rangeRaycast, interactableMask))
        {
            var selection = hit.transform;
            if (selection.GetComponent<Interactable>()!=null)
            {
                Interactable interactable = selection.GetComponent<Interactable>();
                hooverText.UpdateHooverText(interactable.prompt);
                var selectionRenderer=selection.GetComponent<Renderer>();
                if(selectionRenderer!=null)
                    selectionRenderer.material.color = Color.cyan;

                interactable.gameObject.GetComponent<Renderer>().material.color = Color.cyan;
                if (input.playerOnFootActions.Interact.triggered)
                    interactable.BaseInteract();
            }
            selectedObject = selection;
        }
    }
}
