using UnityEngine;

public class Drawer : Interactable
{ 
    private AudioManagerScript audioManagerScript; 
    [SerializeField] private Animator animator;
   private void Start()
   {
       audioManagerScript = GameObject.Find("AudioManager").GetComponent<AudioManagerScript>();
   }

   protected override void Interact()
   {
       audioManagerScript.SoundPlay("DrawerSound");
       animator.SetTrigger("Use");
   }
}
