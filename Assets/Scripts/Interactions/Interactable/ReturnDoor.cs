using System;
using UnityEngine;

public class ReturnDoor : Interactable
{
    public static event Action returnDoorOpen;
    private AudioManagerScript audioManagerScript;

    private void Start()
    {
        audioManagerScript = GameObject.Find("AudioManager").GetComponent<AudioManagerScript>();
        AreYouSure.ReturnDoorIsOpened += OnReturnDoorIsOpened;
    }

    protected override void Interact()
    {
        returnDoorOpen?.Invoke();
    }

    private void OnReturnDoorIsOpened()
    {
        audioManagerScript.SoundPlay("RoomDoor");
    }

    private void OnDestroy()
    {
        AreYouSure.ReturnDoorIsOpened -= OnReturnDoorIsOpened;
    }
}