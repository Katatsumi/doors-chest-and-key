using System;
using UnityEngine;

public class RoomDoor : Interactable
{
   public static event Action roomDoorOpen;
   private AudioManagerScript audioManagerScript;

   private void Start()
   {
      audioManagerScript = GameObject.Find("AudioManager").GetComponent<AudioManagerScript>();
      AreYouSure.SecondRoomIsOpened += OnRoomDoorIsOpened;
   }

   protected override void Interact()
   {
      roomDoorOpen?.Invoke();
   }

   private void OnRoomDoorIsOpened()
   {
      audioManagerScript.SoundPlay("RoomDoor");

   }
   private void OnDestroy()
   { 
      AreYouSure.SecondRoomIsOpened -= OnRoomDoorIsOpened;
   }
}
