using UnityEngine;
using System;

public class Door : Interactable
{
    private AudioManagerScript audioManagerScript;
    public static event Action doorOpen;
    [HideInInspector] public bool playerHasAKey;
    private void Start()
    {
        audioManagerScript = GameObject.Find("AudioManager").GetComponent<AudioManagerScript>();
        doorOpen += KeySoundCheck;
    }
    protected override void Interact()
    {
        doorOpen?.Invoke();
    }
    private void KeySoundCheck()
    {
        if (playerHasAKey)
            audioManagerScript.SoundPlay("KeySound");

    }
    private void OnDestroy()
    {
        doorOpen -= KeySoundCheck;
    }
}
