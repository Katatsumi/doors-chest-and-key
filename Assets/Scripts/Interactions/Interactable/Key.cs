using UnityEngine;
using System;

public class Key : Interactable
{
    public static event Action OnPickUpAction;

    [SerializeField] public MeshRenderer keyMesh;
    [SerializeField] public BoxCollider keyCollider;
    [SerializeField] public ParticleSystem keyParticles;
    public void OnEnable()
    {
        AreYouSure.KeyIsPickedUp += KeyTaken;
    }
    protected override void Interact()
    {
        OnPickUpAction?.Invoke();
    }
    private void OnDestroy()
    {
        AreYouSure.KeyIsPickedUp -= KeyTaken;
    }
    private void KeyTaken()
    {
        keyMesh.enabled = false;
        keyCollider.enabled = false;
        keyParticles.gameObject.SetActive(false);
    }
    
}
