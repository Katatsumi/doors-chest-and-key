using UnityEngine;
using TMPro;
public class HooverText : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI hooverText;
    private void Start()
    {
        UpdateHooverText(string.Empty);
    }
    public void UpdateHooverText(string text)
    {
        hooverText.text = text;
    }    

}
