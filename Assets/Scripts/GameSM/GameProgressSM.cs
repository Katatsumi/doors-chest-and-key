using UnityEngine;

public class GameProgressSM : StateMachine
{
    [HideInInspector]
    public IdleState idleState;
    [HideInInspector]
    public StartState startState;
    [HideInInspector]
    public GotKeyState gotKeyState;
    [HideInInspector]
    public EndState endState;

    private void Awake()
    {
        idleState=new IdleState(this);
        startState = new StartState(this);
        gotKeyState = new GotKeyState(this);
        endState = new EndState(this);
    }

    protected override BaseState GetInitialState()
    {
        return idleState;
    }

}
