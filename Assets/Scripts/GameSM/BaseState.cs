public class BaseState
{
    protected StateMachine machine;

    public BaseState(StateMachine stateMachine)
    {
        machine = stateMachine;
    }
    public virtual void StateEnter() { }
    public virtual void StateExit() { }
    public virtual void StateUpdateLogic() { }
    public virtual void StateUpdatePhysics() { }

}
