using UnityEngine;

public class GotKeyState : BaseState
{
    public GotKeyState(StateMachine stateMachine) : base(stateMachine) { }
    private Door door;
    private AreYouSure areYouSure;

    public override void StateEnter()
    {
        AreYouSure.DoorIsOpened += () => { machine.ChangeState(((GameProgressSM)machine).endState); };
        door=GameObject.FindWithTag("Door").GetComponent<Door>();
        areYouSure=GameObject.Find("UI").GetComponent<AreYouSure>();
        door.playerHasAKey = true;
        areYouSure.playerHasAKey = true;



    }
}
