public class StartState : BaseState
{
    public StartState(StateMachine stateMachine) : base (stateMachine) { }

    public override void StateEnter()
    {
        AreYouSure.KeyIsPickedUp += () => { machine.ChangeState(((GameProgressSM)machine).gotKeyState);};
    }
}