using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class EndState : BaseState
{
    public EndState(StateMachine stateMachine) : base(stateMachine) { }

    public override void StateEnter()
    {
        GameOverMenu.GameRestart += () =>
        {
        machine.ChangeState(((GameProgressSM)machine).idleState);
        };
    }
}
