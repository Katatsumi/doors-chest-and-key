using UnityEngine;

public class IdleState : BaseState
{
    public IdleState(StateMachine stateMachine) : base(stateMachine) { }

    public override void StateEnter()
    {
        StartButton.GameStarted+= () => {machine.ChangeState(((GameProgressSM)machine).startState);};
    }
}
