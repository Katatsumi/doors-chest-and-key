using UnityEngine;

public class StateMachine : MonoBehaviour
{
    private BaseState currentState;

    private void Start()
    {
        if (currentState == null)
        {
            currentState = GetInitialState();
            currentState.StateEnter();

        }
    }
    private void Update()
    {
       if(currentState!=null)
        currentState.StateUpdateLogic();
    }
    private void FixedUpdate()
    {
        if (currentState != null)
            currentState.StateUpdatePhysics();
    }

    public void ChangeState(BaseState state)
    {
        currentState.StateExit();
        currentState = state;
        currentState.StateEnter();
    }

    protected virtual BaseState GetInitialState()
    {
        return null;
    }
}
