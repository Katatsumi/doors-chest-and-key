using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField] Button tryAgainButton;
    [SerializeField] Image startMenuPanel;
    [SerializeField] Image gameOverMenu;
    [SerializeField] GameObject door;
    [SerializeField] GameObject roomDoor;
    [SerializeField] GameObject drawer;
    [SerializeField] Animator doorAnimator;
    [SerializeField] SetRandomPostion randomPostion;
    [SerializeField] Timer timer;
    [SerializeField] KeyRandomPlacementScript keyRandomPlacement;
    [SerializeField] Key keyScript;
    [SerializeField] Door doorObject;
    [SerializeField] AreYouSure areYouSureObject;
    [SerializeField] Interaction interaction;
    [SerializeField] Score scoreScript;
    [SerializeField] TextMeshProUGUI interactionText;
    [SerializeField] GameObject player;
    [SerializeField] StartButton startButton;
    [SerializeField] List<Animator> drawersAnimators;

    public static event Action GameRestart;

    void OnEnable()
    {
        
        drawer.GetComponent<SetRandomPostion>().enabled = false;
        timer.timerActive = false;
        scoreScript.enabled = true;
        tryAgainButton.onClick.AddListener(() =>
        {
            GameRestart?.Invoke();
            startButton.enabled = true;
            gameOverMenu.gameObject.SetActive(false);
            startMenuPanel.gameObject.SetActive(true);
            randomPostion.SetRandomPosion(door);
            randomPostion.SetRandomPosion(roomDoor);
            areYouSureObject.playerHasAKey = false;
            doorObject.playerHasAKey = false;
            drawer.GetComponent<SetRandomPostion>().enabled = true;
            keyRandomPlacement.KeySetRandomDrawer();
            keyScript.keyMesh.enabled = true;
            keyScript.keyCollider.enabled = true;
            keyScript.keyParticles.gameObject.SetActive(true);
            interaction.enabled = true;
            interactionText.gameObject.SetActive(true);
            player.transform.position = new Vector3(-3.95f, 0.709f, -11.98f);
            player.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
            doorAnimator.Play("DoorState", 0, 0.0f);
            foreach (Animator animations in drawersAnimators)
            {
               animations.Play("DrawerState",0, 0.0f);
            }
            timer.enabled = false;
            timer.minutes = 0;
            timer.seconds = 0;
        });
    }
}