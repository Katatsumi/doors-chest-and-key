using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.InputSystem;

public class StartButton : MonoBehaviour
{
    [SerializeField] Image startMenuPanel;
    [SerializeField] Image hudPanel;
    [SerializeField] GameObject timerObject;
    [SerializeField] Button startButton;
    [SerializeField] GameObject player;
    [SerializeField] TextMeshProUGUI bestScore;
    [SerializeField] MouseCursor mouseCursor;
    [SerializeField] GameOverMenu gameOverMenuScript;
    [SerializeField] Score scoreScript;
    private Timer timer;
    private PlayerInput playerInput;
    public static event Action GameStarted;
    public void OnEnable()
    {
        mouseCursor.arrowrCursor();
        hudPanel.gameObject.SetActive(false);
        startMenuPanel.gameObject.SetActive(true);
        timer = timerObject.GetComponent<Timer>();
        timer.enabled = false;
        playerInput = player.GetComponent<PlayerInput>();
        if (!PlayerPrefs.HasKey("bestScoreString"))
        {
            PlayerPrefs.SetString("bestScoreString", "00:00");
        }
        bestScore.text = "Best score: " + PlayerPrefs.GetString("bestScoreString");
        startButton.onClick.AddListener(() =>
        {
           mouseCursor.crosshairCursor();
            hudPanel.gameObject.SetActive(true);
            startMenuPanel.gameObject.SetActive(false);
            timer.timerActive = true;
            timer.timerText.text = "0:0";
            timer.enabled = true;
            playerInput.enabled = true;
            gameOverMenuScript.enabled = false;
            GameStarted?.Invoke();
            this.enabled = false;
            scoreScript.enabled = false;
        });
    }
}
