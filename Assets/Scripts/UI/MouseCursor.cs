using UnityEngine;

public class MouseCursor : MonoBehaviour
{
    [SerializeField] Texture2D cursorArrow;
    [SerializeField] Texture2D cursorCrosshair;

    public void crosshairCursor()
    {
        Cursor.SetCursor(cursorCrosshair, Vector2.zero, CursorMode.ForceSoftware);
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void arrowrCursor()
    {
        Cursor.SetCursor(cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
        Cursor.lockState = CursorLockMode.Confined;
    }
}
