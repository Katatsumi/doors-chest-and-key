using UnityEngine;
using UnityEngine.UI;
using System;
using System.Threading;
using TMPro;
public class AreYouSure : MonoBehaviour
{
    [SerializeField] Button yesButton;
    [SerializeField] Button yesOpenButton;
    [SerializeField] Button okButton;
    [SerializeField] Button noButton;
    [SerializeField] GameObject areYouSurePanel;
    [SerializeField] TextMeshProUGUI areYouSureText;
    [SerializeField] GameObject gameOverMenu;
    [SerializeField] PlayerInput playerInput;
    [SerializeField] Interaction interaction;
    [SerializeField] TextMeshProUGUI interactionText;
    [SerializeField] Timer timer;
    [SerializeField] Score score;
    [SerializeField] MouseCursor mouseCursor;
    [SerializeField] GameObject player;
    [SerializeField] GameObject cube;
    [SerializeField] GameOverMenu gameOverMenuScript;
    public static event Action KeyIsPickedUp;
    public static event Action DoorIsOpened;
    public static event Action SecondRoomIsOpened;
    public static event Action ReturnDoorIsOpened;

    
    [HideInInspector] public bool playerHasAKey;
    

    private void Start()
    {
        yesButton.onClick.AddListener(PickUpKey);
        yesOpenButton.onClick.AddListener(EnableThingsDisabledByPopup);
        noButton.onClick.AddListener(EnableThingsDisabledByPopup);
        okButton.onClick.AddListener(EnableThingsDisabledByPopup);
        Key.OnPickUpAction += PickUpKey;
        Door.doorOpen += OpenDoor;
        RoomDoor.roomDoorOpen += OpenRoomDoor;
        ReturnDoor.returnDoorOpen += OpenReturnDoor;
    }

    private void OpenReturnDoor()
    {
        if (playerHasAKey)
        {
            DisableThingsForPopUp();
            areYouSurePanel.SetActive(true);
            areYouSureText.text = "Do you want to go back?";
            yesOpenButton.gameObject.SetActive(true);
            noButton.gameObject.SetActive(true);
            yesButton.gameObject.SetActive(false);
            okButton.gameObject.SetActive(false);
            yesOpenButton.onClick.RemoveAllListeners();
            yesOpenButton.onClick.AddListener(() =>
            {
                Vector3 coordinates = cube.transform.position;
                player.GetComponent<Transform>().position = new Vector3(coordinates.x, 0.709f, coordinates.z);
                player.transform.rotation = Quaternion.Euler(player.transform.rotation.x,ReturnDoorCameraValidator(),player.transform.rotation.z);
                ReturnDoorIsOpened?.Invoke();
                areYouSurePanel.SetActive(false);
            });
        }
        else
        {
            DisableThingsForPopUp();
            areYouSurePanel.SetActive(true);
            areYouSureText.text = "Take the key and then come back";
            yesOpenButton.gameObject.SetActive(false);
            yesButton.gameObject.SetActive(false);
            noButton.gameObject.SetActive(false);
            okButton.gameObject.SetActive(true);
        }
    }

    private float ReturnDoorCameraValidator()
    {
        float angel;
        if (player.transform.position.x <=-7.15)
        {
            angel = 90f;
        } else if (player.transform.position.x>=1.35)
        {
            angel = -90f;
        } else if (player.transform.position.z<=-14)
        {
            angel = 0f;
        }
        else
        {
            angel = -180f;
        }

        return angel;
    }
    
    private void OpenRoomDoor()
    {
        areYouSurePanel.SetActive(true);
        if (!playerHasAKey)
        {
            DisableThingsForPopUp();
            areYouSureText.text = "You want to come inside?";
            yesOpenButton.gameObject.SetActive(true);
            noButton.gameObject.SetActive(true);
            yesButton.gameObject.SetActive(false);
            okButton.gameObject.SetActive(false);
            yesOpenButton.onClick.RemoveAllListeners();
            yesOpenButton.onClick.AddListener(() =>
            {
                player.GetComponent<Transform>().position = new Vector3(8.271f, 0.709f, -8.28f);
                player.transform.rotation = Quaternion.Euler(player.transform.rotation.x,-180,player.transform.rotation.z);
                SecondRoomIsOpened?.Invoke();
                areYouSurePanel.SetActive(false);
            });
        }
        else
        {
            areYouSurePanel.SetActive(true);
            DisableThingsForPopUp();
            areYouSureText.text = "You already have the key";
            yesOpenButton.gameObject.SetActive(false);
            yesButton.gameObject.SetActive(false);
            noButton.gameObject.SetActive(false);
            okButton.gameObject.SetActive(true);
        }
    }

    private void PickUpKey()
    {
        areYouSurePanel.SetActive(true);
        DisableThingsForPopUp();
        areYouSureText.text = "Pick up?";
        yesOpenButton.gameObject.SetActive(false);
        yesButton.gameObject.SetActive(true);
        noButton.gameObject.SetActive(true);
        okButton.gameObject.SetActive(false);
        yesButton.onClick.RemoveAllListeners();
        yesButton.onClick.AddListener(() => 
            { KeyIsPickedUp?.Invoke(); areYouSurePanel.SetActive(false); EnableThingsDisabledByPopup(); });


    }
    private void OpenDoor()
    {
        areYouSurePanel.SetActive(true);
        if(playerHasAKey)
        {
            areYouSureText.text = "Open?";
            DisableThingsForPopUp();
            yesButton.gameObject.SetActive(true);
            yesOpenButton.gameObject.SetActive(false);
            noButton.gameObject.SetActive(true);
            okButton.gameObject.SetActive(false);
            yesButton.onClick.RemoveAllListeners();
            yesButton.onClick.AddListener(() => {
                DoorIsOpened?.Invoke();
                areYouSurePanel.SetActive(false);
                score.enabled = true;
                gameOverMenu.SetActive(true);
                interactionText.text = "";
                timer.enabled = false;
                timer.timerActive = false;
                timer.timerText.text = "";
                gameOverMenuScript.enabled = true;
            });
        }
        else
        {
            areYouSurePanel.SetActive(true);
            DisableThingsForPopUp();
            areYouSureText.text = "You need a key!";
            yesButton.gameObject.SetActive(false);
            yesOpenButton.gameObject.SetActive(false);
            noButton.gameObject.SetActive(false);
            okButton.gameObject.SetActive(true);
        }
    }
    private void EnableThingsDisabledByPopup()
    {
        mouseCursor.crosshairCursor();
        playerInput.enabled = true;
        interaction.enabled = true;
        interactionText.gameObject.SetActive(true);
    }
    private void DisableThingsForPopUp()
    {
        mouseCursor.arrowrCursor();
        playerInput.enabled = false;
        interaction.enabled = false;
        interactionText.gameObject.SetActive(false);
    }
    private void OnDestroy()
    {
        Key.OnPickUpAction -= PickUpKey;
        Door.doorOpen -= OpenDoor;
        RoomDoor.roomDoorOpen -= OpenRoomDoor;
        ReturnDoor.returnDoorOpen -= OpenReturnDoor;
    }
}
