using System;
using System.Collections;
using TMPro;
using UnityEngine;
public class Timer : MonoBehaviour
{
    public int seconds;
    public int minutes;
    public bool timerActive = false;
    public TextMeshProUGUI timerText;

    public void OnEnable()
    {
        seconds = 0;
        StartCoroutine(TimerCoroutine());
    }

    public void OnDisable()
    {
        StopCoroutine(TimerCoroutine());
    }

    void Update()
    {
        if (seconds == 60)
        {
            minutes++;
            seconds = 0;
        }
        timerText.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }
    public IEnumerator TimerCoroutine()
    {
        while (timerActive)
        {
            yield return new WaitForSeconds(1);
            if (timerActive)
            {
                seconds++;
            }
        }
        
    }
}
