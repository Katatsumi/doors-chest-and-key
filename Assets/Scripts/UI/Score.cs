using System;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI currentScore;
    [SerializeField] TextMeshProUGUI bestScore;
    [SerializeField] Timer timer;
    private String finalTime = "";
    private int seconds;

    void OnEnable()
    {
        seconds = timer.minutes * 60 + timer.seconds;
        finalTime = timer.timerText.text;
        if (PlayerPrefs.GetInt("bestScore") > seconds || !PlayerPrefs.HasKey("bestScore"))
        {
            PlayerPrefs.SetInt("bestScore", seconds);
            PlayerPrefs.SetString("bestScoreString", finalTime);
            bestScore.text = "Best score: " + PlayerPrefs.GetString("bestScoreString");
        }
        else
        {
            bestScore.text = "Best score: " + PlayerPrefs.GetString("bestScoreString");
        }
        currentScore.text = "Current score: " + finalTime;
    }
    
}
