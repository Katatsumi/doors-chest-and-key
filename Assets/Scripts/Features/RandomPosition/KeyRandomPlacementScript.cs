using UnityEngine;

public class KeyRandomPlacementScript : MonoBehaviour
{
    [SerializeField]
    private GameObject[] drawers = new GameObject[9];
    private Vector3 keyPositionBugfix = new Vector3(0f, 0.36f, -0.609f);



    private void Start()
    {
        KeySetRandomDrawer();
    }

    public void KeySetRandomDrawer()
    {
        int randomnumber = Random.Range(0, 8);
        Transform keyTransform = this.gameObject.transform;
        keyTransform.SetParent(drawers[randomnumber].transform);
        if (randomnumber == 7 || randomnumber == 8) // Couldn't fix the problem with the weird transform of the bottom drawers.
            keyTransform.localPosition = keyPositionBugfix;
        else
            keyTransform.localPosition = Vector3.zero;
    }
}
