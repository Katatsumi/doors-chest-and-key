using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomDoorCollisionCheck : MonoBehaviour
{
    [SerializeField] private SetRandomPostion setRandomPostion;
    [SerializeField] private GameObject roomDoor;
    private void OnTriggerStay(Collider other)
    {
        if(other.tag=="Door")
        {
            setRandomPostion.SetRandomPosion(roomDoor);
        }
      
    }
}
