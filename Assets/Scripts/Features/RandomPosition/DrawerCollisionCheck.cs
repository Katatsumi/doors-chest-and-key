using UnityEngine;

public class DrawerCollisionCheck : MonoBehaviour
{
    [SerializeField] private SetRandomPostion setRandomPostion;
    [SerializeField] private GameObject drawer;
    private void OnTriggerStay(Collider other)
    {
        if(other.tag=="InsideRoomDoor")
        {
            setRandomPostion.SetRandomPosion(drawer);
        }
      
    }
}
