using UnityEngine;

public class SetRandomPostion : MonoBehaviour
{
    [SerializeField] PositionSO leftWallObjectPostion;
    [SerializeField] PositionSO rightWallObjectPostion;
    [SerializeField] PositionSO topWallObjectPostion;
    [SerializeField] PositionSO bottomWallObjectPostion;
    [SerializeField] RotationSO objectRotation;
    private enum Walls
    {
        left,
        right,
        top,
        bottom 
    }
    private Walls randomWall;
    private void OnEnable()
    {
        SetRandomPosion(this.gameObject);
    }
    public void SetRandomPosion(GameObject thing)
    {
        randomWall = (Walls)Random.Range(0, 3);
        switch(randomWall)
        {
            case Walls.left:
                thing.transform.position = new Vector3
                    (Random.Range(leftWallObjectPostion.xMin, leftWallObjectPostion.xMax), leftWallObjectPostion.y, leftWallObjectPostion.zMin);
                thing.transform.rotation = Quaternion.Euler(objectRotation.leftRotation);
                break;
            case Walls.right:
                thing.transform.position = new Vector3
                    (Random.Range(rightWallObjectPostion.xMin, rightWallObjectPostion.xMax), rightWallObjectPostion.y, rightWallObjectPostion.zMin) ;
                thing.transform.rotation = Quaternion.Euler(objectRotation.rightRotation);
                break;
            case Walls.top:
                thing.transform.position = new Vector3
                    (topWallObjectPostion.xMin, topWallObjectPostion.y, Random.Range(topWallObjectPostion.zMin, topWallObjectPostion.zMax));
                thing.transform.rotation = Quaternion.Euler(objectRotation.topRotation);
                break;
            case Walls.bottom:
                thing.transform.position = new Vector3
                    (bottomWallObjectPostion.xMin, bottomWallObjectPostion.y, Random.Range(bottomWallObjectPostion.zMin, bottomWallObjectPostion.zMax));
                thing.transform.rotation = Quaternion.Euler(objectRotation.bottomRotation);
                break;
        }

    }
}
