using UnityEngine;

public class DoorAnimation : MonoBehaviour
{
    [SerializeField]
    private Animator doorAnimator;
    private AudioManagerScript audioManagerScript;
    void Start()
    {
        audioManagerScript = GameObject.Find("AudioManager").GetComponent<AudioManagerScript>();
        AreYouSure.DoorIsOpened += OnDoorIsOpened;
    }
    private void OnDoorIsOpened()
    {
        doorAnimator.SetTrigger("Use");
        audioManagerScript.SoundPlay("DoorSound");

    }
    private void OnDestroy()
    {
        AreYouSure.DoorIsOpened -= OnDoorIsOpened;
    }

}
